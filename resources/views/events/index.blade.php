<x-app-layout>
	<div class="container">
		<div class="row">
			<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
				<div class="d-block mb-md-0">
					<h2 class="h4">Event Schedule</h2>
				</div>
				<div class="btn-toolbar mb-2 mb-md-0">
					<a href="{{ route('events.create') }}" class="btn btn-primary rounded-pill px-3 d-inline-flex align-items-center">
						Schedule Event
					</a>
				</div>
			</div>
		</div>
		<div>
			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Title</th>
						<th scope="col">Description</th>
						<th scope="col">Location</th>
						<th scope="col">Start Date</th>
						<th scope="col">End Date</th>
						<th scope="col">Recurring</th>
						<th scope="col">Reminder Date</th>
					</tr>
				</thead>
				<tbody>
					{{-- @foreach ($events as $event)
            <tr>
                <td>{{ $event->title }}</td>
                <td>{{ $event->description }}</td>
                <td>{{ $event->location }}</td>
                <td>{{ $event->start_datetime }}</td>
                <td>{{ $event->end_datetime }}</td>
                <td>{{ $event->recurring }}</td>
                <td>{{ $event->reminder_datetime }}</td>
            </tr>
        @endforeach --}}
				</tbody>
			</table>
		</div>
	</div>
</x-app-layout>