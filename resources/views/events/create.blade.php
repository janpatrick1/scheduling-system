<x-app-layout>
	<div class="container mt-3">
				<form method="POST" action="{{ route('events.store') }}">
					@csrf
					<!-- 2 column grid layout with text inputs for the first and last names -->
					<div class="row mb-4">
						<div class="col-xl-4">
							<div class="form-outline">
								<input type="text" id="title" class="form-control" />
								<label class="form-label" for="title">Title</label>
							</div>
						</div>
					</div>

					<div class="col-xl-8">
						<div class="form-outline">
							<input type="text" id="description" class="form-control" />
							<label class="form-label" for="description">Desription</label>
						</div>
					</div>
				
					<!-- Location input -->
					<div class="form-outline mb-4">
						<input type="text" id="location" class="form-control" />
						<label class="form-label" for="location">Location</label>
					</div>
					
					<div class="row">
						<div class="col-xl-6">
							<!-- Start Date input -->
							<div class="form-outline mb-4">
								<input type="date" id="start_datetime" class="form-control" />
								<label class="form-label" for="start_datetime">Start Date</label>
							</div>
						</div>
						<div class="col-xl-6">
							<!-- End Date input -->
					<div class="form-outline mb-4">
						<input type="date" id="end_datetime" class="form-control" />
						<label class="form-label" for="end_datetime">End Date</label>
					</div>
						</div>
					</div>
					
					<!-- Recurring input -->
					<div class="form-outline mb-4">
						<input type="text" id="recurring" class="form-control" />
						<label class="form-label" for="recurring">Recurring</label>
					</div>

					<!-- Reminder input -->
					<div class="col-xl-6">
						<div class="form-outline mb-4">
							<input type="date" id="reminder_datetime" class="form-control" />
							<label class="form-label" for="reminder_datetime">Reminder Date</label>
						</div>
					</div>
					
					<!-- Submit button -->
					<button class="btn btn-primary" type="submit">Schedule</button>
				</form>
	</div>
</x-app-layout>