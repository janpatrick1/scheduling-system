<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Event\StoreEventRequest;

use App\Models\Event;
use Illuminate\Http\RedirectResponse;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Event $events)
    {
      return view('events.index',compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('events.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreEventRequest $request):RedirectResponse
    {
			
        $data = $request->validated();
				dd($data->id);

				$event 											= new Event;
				$event->title 							= $data['title'];
				$event->description 				= $data['description'];
				$event->location 						= $data['location'];
				$event->start_datetime 			= $data['start_datetime'];
				$event->end_datetime 				= $data['end_datetime'];
				$event->recurring 					= $data['recurring'];
				$event->reminder_datetime 	= $data['reminder_datetime'];
				$event->save();
				
				
				return redirect()->route('events.index')->with('success', 'Task created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
			$data = $request->validated();

			$event 											= new Event;
			$event->title 							= $data['title'];
			$event->description 				= $data['description'];
			$event->location 						= $data['location'];
			$event->start_datetime 			= $data['start_datetime'];
			$event->end_datetime 				= $data['end_datetime'];
			$event->recurring 					= $data['recurring'];
			$event->reminder_datetime 	= $data['reminder_datetime'];
			$event->update();
			
			return redirect()->route('events.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
