<?php

namespace App\Http\Requests\Event;

use Illuminate\Foundation\Http\FormRequest;

class StoreEventRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 */
	public function authorize(): bool
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
	 */
	public function rules(): array
	{
		return [
      'title'														=> [ 'required', 'string', ],
      'description'											=> [ 'required', 'string' ],
      'location'												=> [ 'required', 'string' ],
			'start_datetime'           				=> ['required', 'date'],
			'end_datetime'          				 	=> ['required', 'date'],
			'recurring'												=> ['required', 'string'],
			'reminder_datetime'								=> ['required',	'date'],
		];
	}
}

